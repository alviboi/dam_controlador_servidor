import java.sql.Timestamp;
import java.util.Date;

import data.VisitaDAO;
import domain.Visita;

/**
 * 
 */

/**
 * @author alviboi
 *
 */
public class proves {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VisitaDAO visitadao = new VisitaDAO();
		Visita vis = new Visita();
		vis.setTotal(4);
		vis.setDefinitius(4);
		vis.setFuncPract(4);
		
		vis.setCFC("CFC");
		vis.setCAF("CAF");
		vis.setLiniaPedagogica("LiniaPedagogica");
		vis.setProjectesDestacables("ProjectesDestacables");
		
		vis.setiMoute(0);
		vis.setErasmus(0);
		
		vis.setPIIE(0);
		vis.setAltresProjectes("AltresProjectes");
		vis.setImpacte("Impacte");
		vis.setParticipacioAF("ParticipacioAF");
		vis.setFIPrimeresImpr("FIPrimeresImpr");
		vis.setFIDificultatsTrobades("FIDificultatsTrobades");
		vis.setFIIDesenvolupament("FIIDesenvolupament");
		vis.setFIIDificultatsTroben("FIIDificultatsTroben");
		vis.setEspais("Espais");
		vis.setCodi(46012094);
			
		Timestamp a = Timestamp.valueOf("2000-12-12 12:12:12");
		vis.setData(a);
		vis.setUser_id(20);
		visitadao.save_visita(vis);
	}

}
