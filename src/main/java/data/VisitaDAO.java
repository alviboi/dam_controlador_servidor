package data;
import util.*;
import domain.*;
import jakarta.persistence.criteria.CriteriaQuery;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
public class VisitaDAO {

	public Visita get_Visita (int codi, Date data) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		Visita visit = new Visita();
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			visit = (Visita) session.createQuery("FROM Visita WHERE codi = :codi and data = :data").setParameter("codi", codi).setParameter("data", data).uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return visit;
		
	}
	
	
	public void save_visita (Visita visita) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = session.beginTransaction();
		//System.out.println(visita.getData().toString());
		session.saveOrUpdate(visita);
		tx.commit();
		session.close();
		
	}
	
}
