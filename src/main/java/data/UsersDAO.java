package data;
import util.*;
import domain.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
public class UsersDAO {


	/* Method to READ all the users */
	public ArrayList <Users> listusers() {
		Session session = HibernateUtil.factory.openSession();
		ArrayList <Users> Totsusers = new ArrayList <Users> ();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			
			@SuppressWarnings("unchecked")
			List users = session.createQuery("FROM Users").list();
			if (users.isEmpty())
				System.out.println("******** No items found");
			for (Iterator iterator = users.iterator(); iterator.hasNext();) {
				Users centre = (Users) iterator.next();
				Totsusers.add(centre);

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return Totsusers;
	}
	
	
	public Users get_User (String mail) {
		Session session = HibernateUtil.factory.openSession();
		Transaction tx = null;
		Users user = new Users();
		try {
			tx = session.beginTransaction();
			// https://stackoverflow.com/questions/15913150/what-is-the-proper-way-to-cast-hibernate-
			user = (Users) session.createQuery("FROM Users WHERE email = :email").setParameter("email", mail).getSingleResult();
			System.out.println(user.getName());
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return user;
		
	}

}