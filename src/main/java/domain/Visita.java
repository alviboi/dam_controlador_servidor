package domain;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * Classe Visita per a Hibernate
 * 
 * @author alfredo
 *
 */
public class Visita implements Serializable{
	private static final long serialVersionUID = 1L;
	private int Total;
    private int Definitius;
    private int FuncPract;
    private String CFC;
    private String CAF;
    private String LiniaPedagogica;
    private String ProjectesDestacables;
    private int iMoute;
    private int Erasmus;
    private int PIIE;
    private String AltresProjectes;
    private String Impacte;
    private String ParticipacioAF;
    private String FIPrimeresImpr;
    private String FIDificultatsTrobades;
    private String FIIDesenvolupament;
    private String FIIDificultatsTroben;
    private String Espais;
    private Timestamp Data;
    private int user_id;
    private int Codi;

    private Centres centre;

    /**
	 * @return the centre
	 */
	public Centres getCentre() {
		return centre;
	}

	/**
	 * @param centre the centre to set
	 */
	public void setCentre(Centres centre) {
		this.centre = centre;
	}


	public Visita(int total, int definitius, int funcPract, String CFC, String liniaPedagogica, String projectesDestacables, int iMoute, int erasmus, int PIIE, String altresProjectes, String impacte, String participacioAF, String FIPrimeresImpr, String FIDificultatsTrobades, String FIIDesenvolupament, String FIIDificultatsTroben, String Espais, int Codi, Timestamp data, int user_id) {
        super();
        this.Codi = Codi;
        this.Data = data;
    	this.Total = total;
        this.Definitius = definitius;
        this.FuncPract = funcPract;
        this.CFC = CFC;
        //this.CAF = CAF;
        this.LiniaPedagogica = liniaPedagogica;
        this.ProjectesDestacables = projectesDestacables;
        this.iMoute = iMoute;
        this.Erasmus = erasmus;
        this.PIIE = PIIE;
        this.AltresProjectes = altresProjectes;
        this.Impacte = impacte;
        this.ParticipacioAF = participacioAF;
        this.FIPrimeresImpr = FIPrimeresImpr;
        this.FIDificultatsTrobades = FIDificultatsTrobades;
        this.FIIDesenvolupament = FIIDesenvolupament;
        this.FIIDificultatsTroben = FIIDificultatsTroben;
        this.Espais = Espais;
        this.user_id = user_id;
    }

	/**
	 * Constructor sense variables
	 */
	public Visita() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the data
	 */
	public Timestamp getData() {
		return Data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Timestamp data) {
		Data = data;
	}

	/**
	 * @return the user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return Total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		Total = total;
	}

	/**
	 * @return the definitius
	 */
	public int getDefinitius() {
		return Definitius;
	}

	/**
	 * @param definitius the definitius to set
	 */
	public void setDefinitius(int definitius) {
		Definitius = definitius;
	}

	/**
	 * @return the funcPract
	 */
	public int getFuncPract() {
		return FuncPract;
	}

	/**
	 * @param funcPract the funcPract to set
	 */
	public void setFuncPract(int funcPract) {
		FuncPract = funcPract;
	}

	/**
	 * @return the cFC
	 */
	public String getCFC() {
		return CFC;
	}

	/**
	 * @param cFC the cFC to set
	 */
	public void setCFC(String cFC) {
		CFC = cFC;
	}

	/**
	 * @return the cAF
	 */
	public String getCAF() {
		return CAF;
	}

	/**
	 * @param cAF the cAF to set
	 */
	public void setCAF(String cAF) {
		CAF = cAF;
	}

	/**
	 * @return the liniaPedagogica
	 */
	public String getLiniaPedagogica() {
		return LiniaPedagogica;
	}

	/**
	 * @param liniaPedagogica the liniaPedagogica to set
	 */
	public void setLiniaPedagogica(String liniaPedagogica) {
		LiniaPedagogica = liniaPedagogica;
	}

	/**
	 * @return the projectesDestacables
	 */
	public String getProjectesDestacables() {
		return ProjectesDestacables;
	}

	/**
	 * @param projectesDestacables the projectesDestacables to set
	 */
	public void setProjectesDestacables(String projectesDestacables) {
		ProjectesDestacables = projectesDestacables;
	}

	/**
	 * @return the iMoute
	 */
	public int getiMoute() {
		return iMoute;
	}

	/**
	 * @param iMoute the iMoute to set
	 */
	public void setiMoute(int iMoute) {
		this.iMoute = iMoute;
	}

	/**
	 * @return the erasmus
	 */
	public int getErasmus() {
		return Erasmus;
	}

	/**
	 * @param erasmus the erasmus to set
	 */
	public void setErasmus(int erasmus) {
		Erasmus = erasmus;
	}

	/**
	 * @return the pIIE
	 */
	public int getPIIE() {
		return PIIE;
	}

	/**
	 * @param pIIE the pIIE to set
	 */
	public void setPIIE(int pIIE) {
		PIIE = pIIE;
	}

	/**
	 * @return the altresProjectes
	 */
	public String getAltresProjectes() {
		return AltresProjectes;
	}

	/**
	 * @param altresProjectes the altresProjectes to set
	 */
	public void setAltresProjectes(String altresProjectes) {
		AltresProjectes = altresProjectes;
	}

	/**
	 * @return the impacte
	 */
	public String getImpacte() {
		return Impacte;
	}

	/**
	 * @param impacte the impacte to set
	 */
	public void setImpacte(String impacte) {
		Impacte = impacte;
	}

	/**
	 * @return the participacioAF
	 */
	public String getParticipacioAF() {
		return ParticipacioAF;
	}

	/**
	 * @param participacioAF the participacioAF to set
	 */
	public void setParticipacioAF(String participacioAF) {
		ParticipacioAF = participacioAF;
	}

	/**
	 * @return the fIPrimeresImpr
	 */
	public String getFIPrimeresImpr() {
		return FIPrimeresImpr;
	}

	/**
	 * @param fIPrimeresImpr the fIPrimeresImpr to set
	 */
	public void setFIPrimeresImpr(String fIPrimeresImpr) {
		FIPrimeresImpr = fIPrimeresImpr;
	}

	/**
	 * @return the fIDificultatsTrobades
	 */
	public String getFIDificultatsTrobades() {
		return FIDificultatsTrobades;
	}

	/**
	 * @param fIDificultatsTrobades the fIDificultatsTrobades to set
	 */
	public void setFIDificultatsTrobades(String fIDificultatsTrobades) {
		FIDificultatsTrobades = fIDificultatsTrobades;
	}

	/**
	 * @return the fIIDesenvolupament
	 */
	public String getFIIDesenvolupament() {
		return FIIDesenvolupament;
	}

	/**
	 * @param fIIDesenvolupament the fIIDesenvolupament to set
	 */
	public void setFIIDesenvolupament(String fIIDesenvolupament) {
		FIIDesenvolupament = fIIDesenvolupament;
	}

	/**
	 * @return the fIIDificultatsTroben
	 */
	public String getFIIDificultatsTroben() {
		return FIIDificultatsTroben;
	}

	/**
	 * @param fIIDificultatsTroben the fIIDificultatsTroben to set
	 */
	public void setFIIDificultatsTroben(String fIIDificultatsTroben) {
		FIIDificultatsTroben = fIIDificultatsTroben;
	}

	/**
	 * @return the espais
	 */
	public String getEspais() {
		return Espais;
	}

	/**
	 * @param espais the espais to set
	 */
	public void setEspais(String espais) {
		Espais = espais;
	}

	/**
	 * @return the codi
	 */
	public int getCodi() {
		return Codi;
	}

	/**
	 * @param codi the codi to set
	 */
	public void setCodi(int codi) {
		Codi = codi;
	}
	
	public String getNom() {
		return this.getCentre().getNom();
		//return "Prova";
		
	}

   
}
