import java.io.BufferedReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

import domain.Centres;

/**
 * Clase que gestiona la exritura del socket quan volem enviar els centres (Aquesta clase queda anulada i es gestiona tot des del Fil del servidor)
 * 
 * @author alviboi
 *
 */
public class Escritura extends Thread {

	BufferedReader fentrada;
	BufferedReader linea;
	PrintWriter fsalida;
	ObjectOutputStream outToClient;
	Centres_tots centres;
	Socket socket = null;
	String  a = "";
	Centres este;
	Gson g;  
	public Escritura(Socket s) throws IOException {// CONSTRUCTOR
		socket = s;
		centres = new Centres_tots();
		centres.emplenaObjecte();
		g = new Gson();
		// se crean flujos de entrada y salida
		fsalida = new PrintWriter(socket.getOutputStream(), true);
		fsalida.flush();
		
	}
	public void run() {// tarea a realizar con el cliente
		String cadena = "";
		System.out.println("COMUNICO CON: " + socket.toString());

			//System.out.println(centres.getCentres().get(1).getNom());
			//System.out.println("Escriu: \n");				
			a = g.toJson(centres.getCentres());
			
			do {
				fsalida.println(a);
				fsalida.flush();
				System.out.println(socket.isConnected());
				try {
					sleep(20);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} while (socket.isConnected());
			
			try {
				socket.close();
				System.out.println("TAncada");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
}

