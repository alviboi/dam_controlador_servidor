import domain.*;
import java.io.Serializable;
import java.util.ArrayList;

import org.mindrot.jbcrypt.BCrypt;

import data.CentresDAO;
import data.UsersDAO;

/**
 * Clase per a gestionar tots els centres al mateix temps
 * 
 * @author alviboi
 *
 */
public class Centres_tots implements Serializable {
	ArrayList <Centres> centres;
	CentresDAO centresdao;
	UsersDAO userdao;
	Users user;
	String usuari;
	String contrassenya;
	
	public Centres_tots() {
		centresdao = new CentresDAO();
		userdao = new UsersDAO();
		centres = new ArrayList();
		user = new Users();
	}
	
	
	/**
	 * @return the usuari
	 */
	public String getUsuari() {
		return usuari;
	}

	/**
	 * @param usuari the usuari to set
	 */
	public void setUsuari(String usuari) {
		this.usuari = usuari;
	}

	/**
	 * @return the contrassenya
	 */
	public String getContrassenya() {
		return contrassenya;
	}

	/**
	 * @param contrassenya the contrassenya to set
	 */
	public void setContrassenya(String contrassenya) {
		this.contrassenya = contrassenya;
	}

	/**
	 * @return the centres
	 */
	public ArrayList<Centres> getCentres() {
		return centres;
	}

	/**
	 * @param centres the centres to set
	 */
	public void setCentres(ArrayList<Centres> centres) {
		this.centres = centres;
	}
	
	public void addCentre (Centres centre) {
		centres.add(centre);
	}
	
	public void emplenaObjecte() {
		centres = centresdao.listcentres();
	}
	
	public Boolean comprova_usuari() {
		user = userdao.get_User(usuari);
		String password_hash = "$2a" +  user.getPassword().substring(3);
		boolean result1 = BCrypt.checkpw(contrassenya, password_hash);
		return result1;
	}
	
	public ArrayList<Centres> get_centres_usuari(String usuari){
		ArrayList <Centres> centres = new ArrayList <Centres>();
		this.setUsuari(usuari);
		if (this.comprova_usuari()) {
			centres = centresdao.listcentresusuari(user.getId());
		}
		return centres;
	}

}
