import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;
import org.json.JSONArray;
import data.UsersDAO;
import data.VisitaDAO;
import domain.Visita;

/**
 * Fil del servidor per a gestionar totes les comnunicacions
 * 
 * @author alviboi
 *
 */
public class HiloServidor extends Thread {

	BufferedReader fentrada;
	PrintWriter fsalida;
	Socket socket = null;
	Centres_tots centres_tots;
	String  send_serialize = "";
	Gson g;  
	public HiloServidor(Socket s) throws IOException {// CONSTRUCTOR
		socket = s;
		// se crean flujos de entrada y salida
		fsalida = new PrintWriter(socket.getOutputStream(), true);
		fsalida.flush();
		fentrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		centres_tots = new Centres_tots();

	    GsonBuilder gb = new GsonBuilder(); 
	    gb.serializeNulls();
		g = gb.create();
	}
	public void run() {// tarea a realizar con el cliente
		String cadena = "";
		System.out.println("COMUNICO CON: " + socket.toString());
		//while (socket.isConnected()) {
			try {
				cadena = fentrada.readLine();
				System.out.println(cadena);				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			String desencriptat = AESUtilitats.decrypt(cadena, "CEFIRE");
			String usuari2 = desencriptat.substring(desencriptat.indexOf("Usuari: ")+8,desencriptat.lastIndexOf(" passwd: "));
			System.out.println(usuari2);
			String contrassenya2 = desencriptat.substring(desencriptat.indexOf("passwd: ")+8,desencriptat.lastIndexOf(" command: "));
			System.out.println(contrassenya2);
			String command2 = desencriptat.substring(desencriptat.indexOf("command: ")+9);
			System.out.println(command2);
			centres_tots.setUsuari(usuari2);
			centres_tots.setContrassenya(contrassenya2);
			System.out.println(centres_tots.comprova_usuari());
			
			if (command2.equals("SincroCentres")) {
				if (centres_tots.comprova_usuari()) {
					send_serialize = g.toJson(centres_tots.get_centres_usuari(usuari2));
					fsalida.println(send_serialize);
					fsalida.flush();
					System.out.println(send_serialize);
					}
				} else if (command2.toString().equals("SincroVisites")) {
					
					    
						UsersDAO user = new UsersDAO();
						VisitaDAO visitadao = new VisitaDAO();
					
						System.out.println("Entre on toca");
						try {
							cadena = fentrada.readLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println(cadena);
												
						
						JSONArray arr_visites = new JSONArray(cadena);
						System.out.println(arr_visites.get(0).toString());
						for (Object object : arr_visites) {
							Visita vis = new Visita();
							JSONObject o = (JSONObject) object;
							//System.out.println(o.get("Impacte").toString());
							vis.setTotal(Integer.parseInt(o.get("Total").toString()));
							vis.setDefinitius(Integer.parseInt(o.get("Definitius").toString()));
							vis.setFuncPract(Integer.parseInt(o.get("FuncPract").toString()));
							
							vis.setCFC(o.get("CFC").toString());
							vis.setCAF(o.get("CAF").toString());
							vis.setLiniaPedagogica(o.get("LiniaPedagogica").toString());
							vis.setProjectesDestacables(o.get("ProjectesDestacables").toString());
							
							vis.setiMoute(Integer.parseInt(o.get("iMoute").toString()));
							vis.setErasmus(Integer.parseInt(o.get("Erasmus").toString()));
							
							vis.setPIIE(Integer.parseInt(o.get("PIIE").toString()));
							vis.setAltresProjectes(o.get("AltresProjectes").toString());
							vis.setImpacte(o.get("Impacte").toString());
							vis.setParticipacioAF(o.get("ParticipacioAF").toString());
							vis.setFIPrimeresImpr(o.get("FIPrimeresImpr").toString());
							vis.setFIDificultatsTrobades(o.get("FIDificultatsTrobades").toString());
							vis.setFIIDesenvolupament(o.get("FIIDesenvolupament").toString());
							vis.setFIIDificultatsTroben(o.get("FIIDificultatsTroben").toString());
							vis.setEspais(o.get("Espais").toString());
							vis.setCodi(Integer.parseInt(o.get("Codi").toString()));
								
							String[] data = o.get("Data").toString().split(" ")[0].split("-");
							String[] temps = o.get("Data").toString().split(" ")[1].split(":");							
							Timestamp a = Timestamp.valueOf(data[2]+"-"+data[1]+"-"+data[0]+" "+temps[0]+":"+temps[1]+":"+temps[2]);
							System.out.println(a.toString());
							vis.setData(a);
							System.out.println(vis.getData().toString());
							vis.setUser_id((user.get_User(usuari2).getId()));
							
							visitadao.save_visita(vis);
							
							
						}

				}
			}

	}





